package org.example.identifying;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryParseException;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.example.identifying.info.Info;
import org.example.identifying.info.SingleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.function.Function;

public class SinglePropertyVisitor extends InfoMapPropertyVisitor<Property, SingleInfo> {
    private static final Logger logger = LoggerFactory.getLogger(SinglePropertyVisitor.class);
    protected Function<String, QueryExecution> executionFactory;


    SinglePropertyVisitor(File csvOut, Function<String, QueryExecution> executionFactory) {
        super(csvOut);
        this.executionFactory = executionFactory;
    }


    protected boolean isIdentifying(Model model, Property predicate, Literal literal,
                                    Resource subject) {
        String lexical = Utils.sparqlEscaped(literal.getLexicalForm());
        String queryStr = String.format(
                "SELECT DISTINCT ?s WHERE {?s <%s> \"%s\"%s.} LIMIT 2",
                predicate.getURI(), lexical, Utils.sparqlSuffix(literal));
        return Utils.withQuery(queryStr, executionFactory, ex -> {
            ResultSet set = ex.execSelect();
            if (!set.hasNext()) throw new NoSuchElementException();
            set.next();
            return !set.hasNext();
        });
    }

    @Override
    public void visit(Resource subject) {
        for (StmtIterator pit = subject.listProperties(); pit.hasNext(); ) {
            Statement ps = pit.next();

            if (!ps.getObject().isLiteral()) continue;
            boolean is;
            Literal lit = ps.getLiteral();
            try {
                is = isIdentifying(subject.getModel(), ps.getPredicate(), lit, subject);
            } catch (NoSuchElementException|QueryParseException e) {
                logger.error("isIdentifying() failed. subj={}, prop={}, lit={}",
                        subject, ps.getPredicate(), lit);
                if (e instanceof QueryParseException) throw e;
                continue;
            }
            updateIdentifying(subject, is, ps.getPredicate(), SingleInfo::new, lit);
        }
    }

    @Override
    public void end() throws IOException {
        super.end(SingleInfo.getHeaders(), Comparator.comparing(SingleInfo::hitRate).reversed());
    }
}
