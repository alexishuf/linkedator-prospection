package org.example.identifying;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {
    private static Logger logger = LoggerFactory.getLogger(Utils.class);

    private static final Pattern bNodePattern = Pattern.compile("_:(.*)");

    public static String semicolonCompress(@Nonnull Collection<?> collection) {
        return collection.stream().map(Object::toString).reduce((a, b) -> a + ";" + b)
                .orElse("");
    }

    public static List<String> semicolonExpand(@Nullable String compressed) {
        if (compressed == null) return Collections.emptyList();
        return Arrays.stream(compressed.split(";")).collect(Collectors.toList());
    }

    public static String sparqlResource(@Nonnull Resource subject) {
        return "<" + resourceToString(subject) + ">";
    }

    public static String resourceToString(@Nonnull Resource resource) {
        if (resource.isAnon())
            return "_:" + resource.getId();
        return URI.create(resource.getURI()).toASCIIString();
    }

    public static Resource resourceFromString(@Nonnull String string) {
        Matcher matcher = bNodePattern.matcher(string);
        if (matcher.matches())
            return new ResourceImpl(AnonId.create(matcher.group(1)));
        return ResourceFactory.createResource(string);
    }

    public static String sparqlSuffix(@Nonnull Literal literal) {
        String lang = literal.getLanguage();
        if (!lang.isEmpty()) return "@" + lang;
        String uri = literal.getDatatypeURI();
        if (uri != null) return "^^<" + uri + ">";
        return "";
    }

    public static String sparqlEscaped(@Nonnull String unescaped) {
        return unescaped.replace("\\", "\\\\")
                .replace("\"", "\\\"")
                .replace("\'", "\\'")
                .replace("\n", "\\b")
                .replace("\t", "\\t");
    }

    public static String sparqlLiteral(@Nonnull Literal lit) {
        return String.format("\"%s\"%s", sparqlEscaped(lit.getLexicalForm()), sparqlSuffix(lit));
    }

    public static <T> T withQuery(String queryStr, Function<String, QueryExecution> executionFactory,
                            Function<QueryExecution, T> function) {
        try (QueryExecution ex = executionFactory.apply(queryStr)) {
            return function.apply(ex);
        } catch (RuntimeException e) {
            logger.error("Exception {} on query: {}", e.getMessage(), queryStr);
            throw e;
        }
    }
}
