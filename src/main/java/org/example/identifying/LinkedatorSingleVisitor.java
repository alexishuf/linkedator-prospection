package org.example.identifying;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDFS;
import org.example.identifying.info.LinkedatorSingleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;

import static org.example.identifying.Utils.sparqlResource;

public class LinkedatorSingleVisitor
        extends LinkedatorVisitor<ImmutablePair<Property, Property>, LinkedatorSingleInfo> {
    private static Logger logger = LoggerFactory.getLogger(LinkedatorSingleVisitor.class);

    LinkedatorSingleVisitor(File csvOut,
                            Function<String, QueryExecution> executionFactory) {
        super(csvOut, executionFactory);
    }

    @Override
    public void visit(Resource subject) {
        if (lockIdentifying) {
            for (StmtIterator it = subject.listProperties(); it.hasNext(); ) {
                Statement ps = it.next();
                if (!ps.getObject().isLiteral()) continue;
                countHits(subject, ps.getPredicate(), ps.getLiteral());
            }
        } else {
            countHits(subject, null, null);
        }
    }

    private String countHitsTemplate() {
        /* ${subj} -- subject SPARQL term
         * ${p} -- identifying predicate URI
         * ${lit} -- literalTerm
         */
        String linkTerm = link == null ? "" : "<" + link.getURI() + ">";
        if (lockIdentifying) {
            if (link == null) {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?other ?link WHERE {\n" +
                        "  ${subj} ?link ?other; a/^rdfs:domain ?link.\n" + // FILTER(${subj} != ?other).\n" +
                        "  ?other ${p} ${lit}; a/^rdfs:range ?link.\n" +
                        "}";
            } else {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?other WHERE {\n" +
                        "  ${subj} " + linkTerm + " ?other.\n" + // FILTER(${subj} != ?other).\n" +
                        "  ?other ${p} ${lit}.\n" +
                        "}";
            }
        } else {
            if (link == null) {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?other ?link ?p1 ?lit1 WHERE {\n" +
                        "  ${subj} ?link ?other; a/^rdfs:domain ?link.\n" + // FILTER(${subj} != ?other).\n" +
                        "  ?other ?p1 ?lit1; a/^rdfs:range ?link FILTER(isLiteral(?lit1)).\n"+
                        "}";
            } else {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?other ?p1 ?lit1 WHERE {\n" +
                        "  ${subj} " + linkTerm + " ?other.\n" + // FILTER(${subj} != ?other).\n" +
                        "  ?other ?p1 ?lit1 FILTER(isLiteral(?lit1)).\n"+
                        "}";
            }
        }
    }

    private void countHits(@Nonnull Resource subject, @Nullable Property predicate,
                           @Nullable Literal literal) {
        Preconditions.checkArgument(!lockIdentifying || (predicate != null && literal != null));
        String predicateTerm = predicate == null ? "" : "<" + predicate.getURI() + ">";
        String litTerm = literal == null ? "" : Utils.sparqlLiteral(literal);
        String subjectTerm = sparqlResource(subject);
        String queryStr = countHitsTemplate().replace("${subj}", subjectTerm)
                .replace("${p}", predicateTerm).replace("${lit}", litTerm);

        Utils.withQuery(queryStr, executionFactory, ex -> {
            ResultSet results = ex.execSelect();
            while (results.hasNext()) {
                QuerySolution sol = results.next();
                if (sol.getResource("other").equals(subject)) continue;
                Property p = predicate != null ? predicate
                        : sol.getResource("p1").as(Property.class);
                Literal lit = literal != null ? literal
                        : sol.getLiteral("lit1").asLiteral();
                Property link = sol.getResource("link").as(Property.class);
                ImmutablePair<Property, Property> k = ImmutablePair.of(p, link);
                updateIdentifying(subject, true, k, LinkedatorSingleInfo::new, lit);
            }
            return null;
        });
    }

    @Override
    public void end() throws IOException {
        super.end(LinkedatorSingleInfo.getHeaders(),
                Comparator.comparing(LinkedatorSingleInfo::getHits).reversed());
    }
}
