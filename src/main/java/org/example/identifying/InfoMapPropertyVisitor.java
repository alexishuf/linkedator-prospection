package org.example.identifying;

import com.google.common.base.Stopwatch;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.RDF;
import org.example.identifying.info.Info;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import static com.google.common.base.Stopwatch.createStarted;

public abstract class InfoMapPropertyVisitor<K, V extends Info> implements Main.Visitor {
    private static final Logger logger = LoggerFactory.getLogger(InfoMapPropertyVisitor.class);
    protected HashMap<K, V> infoMap = new HashMap<>();
    protected File csvOut;

    public InfoMapPropertyVisitor(File csvOut) {
        this.csvOut = csvOut;
    }

    public void end(List<String> headers, Comparator<V> comparator) throws IOException {
        Stopwatch sortWatch = createStarted();
        List<K> list = new ArrayList<>(infoMap.keySet());
        //noinspection SuspiciousMethodCalls
        list.sort(Comparator.comparing(p -> infoMap.get(p), comparator));
        logger.info("Sorted {} properties in {}", list.size(), sortWatch);

        try (FileOutputStream fileOut = new FileOutputStream(csvOut);
             TeeOutputStream tee = new TeeOutputStream(fileOut, System.out);
             OutputStreamWriter writer = new OutputStreamWriter(tee);
             CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
            printer.printRecord(headers);
            for (K p : list)
                printer.printRecord(infoMap.get(p).toStrings());
        }
    }

    protected Info updateIdentifying(Resource subject, boolean is,
                                     K key, Function<? super K, ? extends V> factory,
                                     Literal... literals) {
        Info info = infoMap.computeIfAbsent(key, factory);
        for (Literal lit : literals) {
            if (!lit.getLanguage().isEmpty()) info.getLanguages().add(lit.getLanguage());
            else if (lit.getDatatype() != null) info.getDatatypes().add(lit.getDatatypeURI());
        }
        info.addHits(is ? 1 : 0);
        info.addMisses(!is ? 1 : 0);

        subject.listProperties(RDF.type).toSet().stream()
                .filter(s -> s.getObject().isResource()).map(Statement::getResource)
                .forEach(info.getDomains()::add);
        return info;
    }
}
