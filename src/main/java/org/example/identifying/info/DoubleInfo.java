package org.example.identifying.info;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.Arrays;
import java.util.List;

public class DoubleInfo extends SimpleInfo {
    private Property p1, p2;

    public DoubleInfo(Property p1, Property p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public DoubleInfo(CSVRecord record) {
        super(record);
        p1 = ResourceFactory.createProperty(record.get("p1"));
        p2 = ResourceFactory.createProperty(record.get("p2"));
    }

    public DoubleInfo(ImmutablePair<Property, Property> pair) {
        this(pair.getLeft(), pair.getRight());
    }

    @Override
    public Object getKey() {
        return ImmutablePair.of(p1, p2);
    }

    public static List<String> getHeaders() {
        return SimpleInfo.getHeaders(Arrays.asList("p1", "p2"));
    }

    @Override
    protected List<String> toKeyStrings() {
        return Arrays.asList(p1.getURI(), p2.getURI());
    }
}
