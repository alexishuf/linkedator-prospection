package org.example.identifying.info;

import org.apache.commons.csv.CSVRecord;
import org.apache.jena.rdf.model.Resource;
import org.example.identifying.Utils;

import java.util.*;
import java.util.stream.Collectors;

public abstract class SimpleInfo implements Info {
    protected Set<Resource> domains = new HashSet<>();
    protected Set<String> datatypes = new HashSet<>();
    protected Set<String> languages = new HashSet<>();
    protected long hits;
    protected long misses;


    public SimpleInfo() {
    }

    public SimpleInfo(CSVRecord record) {
        domains = Utils.semicolonExpand(record.get("domains")).stream()
                .map(Utils::resourceFromString).collect(Collectors.toSet());
        datatypes.addAll(Utils.semicolonExpand(record.get("datatypes")));
        datatypes.addAll(Utils.semicolonExpand(record.get("languages")));
        hits = Integer.parseInt(record.get("hits"));
        misses = Integer.parseInt(record.get("misses"));
    }

    @Override
    public Set<Resource> getDomains() {
        return domains;
    }

    @Override
    public Set<String> getDatatypes() {
        return datatypes;
    }

    @Override
    public Set<String> getLanguages() {
        return languages;
    }

    @Override
    public void addHits(long toAdd) {
        hits += toAdd;
    }

    @Override
    public void addMisses(long toAdd) {
        misses += toAdd;
    }

    @Override
    public long getHits() {
        return hits;
    }

    @Override
    public long getMisses() {
        return misses;
    }

    @Override
    public double hitRate() {
        return hits / (double)(hits+misses);
    }

    public static List<String> getHeaders(List<String> keyHeaders) {
        ArrayList<String> l = new ArrayList<>(keyHeaders);
        l.addAll(Arrays.asList("domains", "datatypes", "languages", "hits", "misses", "hitRate"));
        return l;
    }

    protected abstract List<String> toKeyStrings();

    @Override
    public List<String> toStrings() {
        List<String> list = new ArrayList<>();
        list.addAll(toKeyStrings());
        list.addAll(Arrays.asList(
                Utils.semicolonCompress(domains.stream()
                        .map(Utils::resourceToString).collect(Collectors.toList())),
                Utils.semicolonCompress(datatypes), Utils.semicolonCompress(languages),
                String.valueOf(hits), String.valueOf(misses), String.valueOf(hitRate())));
        return list;
    }
}
