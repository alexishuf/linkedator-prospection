package org.example.identifying.info;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.ArrayList;
import java.util.List;

public class LinkedatorSingleInfo  extends SingleInfo {
    private Property link;

    public LinkedatorSingleInfo(Property property, Property link) {
        super(property);
        this.link = link;
    }
    public LinkedatorSingleInfo(ImmutablePair<Property, Property> key) {
        this(key.left, key.right);
    }

    public LinkedatorSingleInfo(CSVRecord record) {
        super(record);
        link = ResourceFactory.createProperty(record.get("link"));
    }

    @Override
    public Object getKey() {
        return ImmutablePair.of(super.getKey(), link);
    }

    public Property getLink() {
        return link;
    }

    @Override
    public List<String> toStrings() {
        List<String> list = new ArrayList<>();
        list.add(link.getURI());
        list.addAll(super.toStrings());
        return list;
    }

    public static List<String> getHeaders() {
        ArrayList<String> list = new ArrayList<>();
        list.add("link");
        list.addAll(SingleInfo.getHeaders());
        return list;
    }
}
