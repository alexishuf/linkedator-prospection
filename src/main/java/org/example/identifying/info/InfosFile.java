package org.example.identifying.info;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import javax.annotation.Nonnull;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class InfosFile {
    public static <T extends Info> List<T> load(@Nonnull File file, Function<CSVRecord, T> loader)
            throws IOException {
        List<T> list = new ArrayList<>();
        try (FileReader reader = new FileReader(file);
             CSVParser parser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader())) {
            for (CSVRecord record : parser) {
                list.add(loader.apply(record));
            }
        }
        return list;
    }
}
