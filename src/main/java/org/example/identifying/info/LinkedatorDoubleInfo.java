package org.example.identifying.info;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

import java.util.ArrayList;
import java.util.List;

public class LinkedatorDoubleInfo extends DoubleInfo {
    private Property link;

    public LinkedatorDoubleInfo(Property p1, Property p2, Property link) {
        super(p1, p2);
        this.link = link;
    }

    public LinkedatorDoubleInfo(ImmutablePair<ImmutablePair<Property, Property>, Property> pair) {
        this(pair.left.left, pair.left.right, pair.right);
    }

    public LinkedatorDoubleInfo(CSVRecord record) {
        super(record);
        link = ResourceFactory.createProperty(record.get("link"));
    }

    @Override
    public Object getKey() {
        return ImmutablePair.of(super.getKey(), link);
    }

    public Property getLink() {
        return link;
    }

    @Override
    public List<String> toStrings() {
        List<String> list = new ArrayList<>();
        list.add(link.getURI());
        list.addAll(super.toStrings());
        return list;
    }

    public static List<String> getHeaders() {
        ArrayList<String> list = new ArrayList<>();
        list.add("link");
        list.addAll(DoubleInfo.getHeaders());
        return list;
    }
}
