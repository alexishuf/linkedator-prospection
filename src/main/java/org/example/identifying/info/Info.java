package org.example.identifying.info;

import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Set;

public interface Info {
    Object getKey();
    Set<Resource> getDomains();
    Set<String> getDatatypes();
    Set<String> getLanguages();
    void addHits(long toAdd);
    void addMisses(long toAdd);

    long getHits();

    long getMisses();

    double hitRate();
    List<String> toStrings();
}
