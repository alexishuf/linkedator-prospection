package org.example.identifying.info;

import org.apache.commons.csv.CSVRecord;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;
import org.example.identifying.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class SingleInfo extends SimpleInfo {
    private Property property;

    public SingleInfo(Property property) {
        this.property = property;
    }

    public SingleInfo(CSVRecord record) {
        super(record);
        property = ResourceFactory.createProperty(record.get("property"));
    }

    @Override
    public Object getKey() {
        return property;
    }

    public Property getProperty() {
        return property;
    }

    public static List<String> getHeaders() {
        return SimpleInfo.getHeaders(Collections.singletonList("property"));
    }

    @Override
    protected List<String> toKeyStrings() {
        return Collections.singletonList(property.getURI());
    }

    @Override
    public List<String> toStrings() {
        return Arrays.asList(property.getURI(), Utils.semicolonCompress(domains),
                Utils.semicolonCompress(datatypes), Utils.semicolonCompress(languages),
                String.valueOf(hits), String.valueOf(misses), String.valueOf(hitRate()));
    }
}
