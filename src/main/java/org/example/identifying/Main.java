package org.example.identifying;

import br.ufsc.inf.lapesd.ld_jaxrs.core.traverser.CBDTraverser;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.impl.model.JenaModelGraph;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.impl.model.JenaNode;
import br.ufsc.inf.lapesd.ld_jaxrs.jena.impl.model.JenaStreamRDFTraverserListener;
import com.google.common.base.Stopwatch;
import org.apache.jena.graph.compose.MultiUnion;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.riot.system.StreamRDF;
import org.apache.jena.riot.system.StreamRDFLib;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.tdb.TDBFactory;
import org.example.identifying.info.InfosFile;
import org.example.identifying.info.SingleInfo;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.base.Stopwatch.createStarted;
import static java.util.concurrent.TimeUnit.*;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static final int PROGRESS_INTERVAL = 10000;

    @Option(name = "--single")
    private File singleCsvOut;

    @Option(name = "--double")
    private File doubleCsvOut;

    @Option(name = "--double-max-pairs")
    private int doubleMaxPairs = Integer.MAX_VALUE;

    @Option(name = "--double-single-csv")
    private File doubleSingleCsv;

    @Option(name = "--double-single-rate-low")
    private double doubleSingleRateLow = 0;

    @Option(name = "--double-single-rate-high")
    private double doubleSingleRateHigh = 1;

    @Option(name = "--linkedator-single")
    private File lkdSingle;

    @Option(name = "--linkedator-single-link")
    private String lkdSingleLink;

    @Option(name = "--linkedator-single-unlock")
    private boolean lkdSingleUnlock;

    @Option(name = "--linkedator-double")
    private File lkdDouble;

    @Option(name = "--linkedator-double-link")
    private String lkdDoubleLink;

    @Option(name = "--linkedator-double-unlock")
    private boolean lkdDoubleUnlock;

    @Option(name = "--prefer-dataset")
    private  boolean preferDataset;

    @Option(name = "--subjects-file", required = true)
    private File subjectsFile;

    @Option(name = "--subjects-count")
    private int subjectCount = Integer.MAX_VALUE;

    @Option(name = "--subset")
    private File subset = null;

    @Option(name = "--subset-subjects-file")
    private File subsetSubjectsFile;

    @Option(name = "--subjects-sample")
    private File subjectsSample;

    @Option(name = "--threads")
    private int threads = Runtime.getRuntime().availableProcessors();

    @Argument(required = true)
    private File[] inputs;


    private long tripleCount;
    private Stopwatch progressTimer;
    private final Pattern prefixedPattern;

    public Main() {
        prefixedPattern = Pattern.compile("([^:]+):([^/].*)");
    }


    public static void main(String[] args) throws Exception {
        NodeValue.VerboseWarnings = false;

        Main app = new Main();
        new CmdLineParser(app).parseArgument(args);
        app.run();
    }

    private void progress(String fmt, Object... args) {
        if (progressTimer == null)
            progressTimer = createStarted();
        if (progressTimer.elapsed(MILLISECONDS) >= PROGRESS_INTERVAL) {
            logger.info(fmt, args);
            progressTimer.reset().start();
        }
    }

    private void run() throws Exception {
        List<Dataset> datasets = new ArrayList<>();
        List<Model> models = new ArrayList<>();
        Model defaultModel = ModelFactory.createDefaultModel();
        Model bigModel = defaultModel;
        try {
            Stopwatch watch = Stopwatch.createStarted();
            for (File input : inputs) {
                if (input.isDirectory()) {
                    Dataset dataset = TDBFactory.createDataset(input.getPath());
                    datasets.add(dataset);
                    Model model = dataset.getDefaultModel();
                    models.add(model);
                    logger.info("Opened TDB at {} with {} triples", input, model.size());
                } else {
                    loadInto(defaultModel, input);
                    logger.info("Loaded {} into default model", input);
                }
            }
            if (models.size() == 1 && defaultModel.isEmpty()) {
                bigModel = models.get(0);
                tripleCount = bigModel.size();
            } else if (!models.isEmpty()) {
                tripleCount = models.stream().map(Model::size).reduce(Long::sum).orElse(0L)
                        + defaultModel.size();
                bigModel = ModelFactory.createModelForGraph(new MultiUnion(
                        Stream.concat(
                                Stream.of(defaultModel), models.stream()
                        ).map(Model::getGraph).iterator()));
            } else {
                tripleCount = bigModel.size();
            }

            logger.info("Loaded {} triples into bigModel in {}", tripleCount, watch);

            Dataset dataset = datasets.size() == 1 ? datasets.get(0) : null;
            if (subjectsSample != null)
                runSubjectsSample(bigModel);
            if (subset != null)
                runSubset(bigModel);
            else
                run(bigModel, dataset);
        } finally {
            bigModel.close();
            datasets.forEach(Dataset::close);

        }
    }

    private void loadInto(Model ontology, File file) throws IOException {
        Lang lang = RDFLanguages.filenameToLang(file.getName());
        if (lang == null)
            throw new IllegalArgumentException("Could not not guess syntax from " + file);
        try (FileInputStream in = new FileInputStream(file)) {
            RDFDataMgr.read(ontology, in, lang);
        }
    }

    public interface Visitor {
        void visit(Resource resource);
        void end() throws IOException;
    }

    private void runSubjectsSample(Model model) throws IOException {
        logger.info("Loading/filling subjects from/to {}", this.subjectsFile);
        SubjectsFile subjectsFile = SubjectsFile.openOrFill(this.subjectsFile, model, p ->
                progress("Scanning triples {}/{} ({}%) {} subjects found in {}",
                        p.done, p.total, Math.round(p.progress()), p.subjects, p.startWatch));

        int subjectsCount = Math.min(subjectCount, subjectsFile.getCount());
        try (SubjectsFile.SampleIterator it = subjectsFile.sample(subjectsCount).inModel(model)) {
            ArrayList<Resource> list = new ArrayList<>(subjectsCount);
            while (it.hasNext()) list.add(it.next());
            SubjectsFile.write(subjectsSample, list);
            logger.info("Wrote sample of {} subjects to {}", subjectsCount, subjectsSample);
        }
    }

    private void runSubset(Model model) throws IOException, InterruptedException {
        logger.info("Loading/filling subjects from/to {}", this.subjectsFile);
        SubjectsFile subjectsFile = SubjectsFile.openOrFill(this.subjectsFile, model, p ->
                progress("Scanning triples {}/{} ({}%) {} subjects found in {}",
                        p.done, p.total, Math.round(p.progress()), p.subjects, p.startWatch));

        int subjectsCount = Math.min(subjectCount, subjectsFile.getCount());
        try (SubjectsFile.SampleIterator it = subjectsFile.sample(subjectsCount).inModel(model)) {
            logger.info("{} subjects to process", subjectsCount);

                List<Resource> selected = new ArrayList<>(subjectsCount);
                while (it.hasNext()) selected.add(it.next());
                if (subsetSubjectsFile != null) {
                    SubjectsFile.write(subsetSubjectsFile, selected);
                    logger.info("Wrote {} subjects of the subset to {}",
                            selected.size(), subsetSubjectsFile);
                }
                createSubsetModel(selected, subset);
                logger.info("Subset written to {} as NTriples", subset);
        }
    }

    private void run(Model model, Dataset dataset) throws IOException, InterruptedException {
        Function<String, QueryExecution> executionFactory;
        executionFactory = q -> QueryExecutionFactory.create(q, model);
        if (dataset != null && preferDataset) {
            logger.info("Will query against the Dataset");
            executionFactory = q -> QueryExecutionFactory.create(q, dataset);
        }

        List<Visitor> visitors = setupVisitors(executionFactory);
        if (visitors.isEmpty()) {
            logger.warn("No visitors!");
            return;
        }

        logger.info("Loading/filling subjects from/to {}", this.subjectsFile);
        SubjectsFile subjectsFile = SubjectsFile.openOrFill(this.subjectsFile, model, p ->
                progress("Scanning triples {}/{} ({}%) {} subjects found in {}",
                        p.done, p.total, Math.round(p.progress()), p.subjects, p.startWatch));

        int subjectsCount = Math.min(subjectCount, subjectsFile.getCount());
        try (SubjectsFile.SampleIterator it = subjectsFile.sample(subjectsCount).inModel(model)) {
            logger.info("{} subjects to process", subjectsCount);
            visit(it, subjectsCount, visitors);
        }
    }

    private void createSubsetModel(List<Resource> selected, File file) throws IOException {
        StreamRDF rdfStream = null;
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(file);
            rdfStream = StreamRDFLib.writer(outStream);

            CBDTraverser traverser = new CBDTraverser()
                    .setSymmetric(true)
                    .setTraverseNamed(true)
                    .setMaxPath(0) //will list properties of who links to x and of who x links to
                    .setReifications(false);
            JenaStreamRDFTraverserListener out = new JenaStreamRDFTraverserListener(rdfStream);
            int done = 0;
            Stopwatch watch = Stopwatch.createStarted();
            for (Resource r : selected) {
                traverser.traverse(new JenaModelGraph(r.getModel()), new JenaNode(r), out);
                ++done;
                long progress = Math.round(done / (double) selected.size() * 100);
                progress("Creating subset model {}/{} ({}%) {} triples, elapsed: {}",
                        done, selected.size(), progress, out.getTripleCount(), watch);
            }

        } finally {
            if (rdfStream != null) rdfStream.finish();
            if (outStream != null) outStream.close();
        }
    }



    private LinkedatorVisitor configureLinkedatorVisitor(@Nonnull LinkedatorVisitor visitor,
                                                         @Nullable String link, boolean unlock) {
        if (link != null) {
            Matcher matcher = prefixedPattern.matcher(link);
            if (matcher.matches()) {
                String prefix = matcher.group(1);
                String prefixURI = PrefixMapping.Standard.getNsPrefixURI(prefix);
                if (prefixURI == null)
                    throw new IllegalArgumentException("Unknown prefix in " + link);
                link = prefixURI + matcher.group(2);
            }
            visitor.setLink(ResourceFactory.createProperty(link));
        }
        visitor.lockIdentifying(!unlock);
        return visitor;
    }

    private List<Visitor>
    setupVisitors(Function<String, QueryExecution> executionFactory) throws IOException {
        List<Visitor> visitors = new ArrayList<>();
        if (singleCsvOut != null)
            visitors.add(new SinglePropertyVisitor(singleCsvOut, executionFactory));
        if (doubleCsvOut != null) {
            DoublePropertyVisitor v = new DoublePropertyVisitor(doubleCsvOut, executionFactory);
            if (doubleSingleCsv != null)
                setWhitelist(v);
            v.setMaxPairs(doubleMaxPairs);
            visitors.add(v);
        }
        if (lkdSingle != null) {
            LinkedatorVisitor v = new LinkedatorSingleVisitor(lkdSingle, executionFactory);
            visitors.add(configureLinkedatorVisitor(v, lkdSingleLink, lkdSingleUnlock));
        }
        if (lkdDouble != null) {
            LinkedatorVisitor v = new LinkedatorDoubleVisitor(lkdDouble, executionFactory);
            visitors.add(configureLinkedatorVisitor(v, lkdDoubleLink, lkdDoubleUnlock));
        }
        return visitors;
    }

    private void setWhitelist(DoublePropertyVisitor v) throws IOException {
        Set<Property> set = InfosFile.load(doubleSingleCsv, SingleInfo::new).stream()
                .filter(i -> i.hitRate() >= doubleSingleRateLow
                        && i.hitRate() < doubleSingleRateHigh)
                .map(SingleInfo::getProperty).collect(Collectors.toSet());
        v.setWhitelist(set);
    }

    private void visit(Iterator<Resource> it, int subjectsCount,
                       List<Visitor> visitors) throws IOException, InterruptedException {
        Stopwatch startWatch = createStarted();
        AtomicInteger done = new AtomicInteger();
        Semaphore semaphore = new Semaphore(threads);
        ExecutorService service = Executors.newFixedThreadPool(threads);
        while (it.hasNext()) {
            Resource subject = it.next();
            while (!semaphore.tryAcquire(PROGRESS_INTERVAL*2, MILLISECONDS))
                logger.warn("Contention in semaphore {} subjects done", done);

            service.submit(() -> {
                try {
                    for (Visitor v : visitors) v.visit(subject);
                } catch (Throwable t) {
                    logger.error("Problem during visit", t);
                } finally {
                    semaphore.release();
                    done.incrementAndGet();
                }
            });
            double prog = done.get() / (double) subjectsCount;
            progress("Processed {}/{} ({}%) subjects, {} elapsed, ETA {}", done, subjectsCount,
                    Math.round(prog*100), startWatch, eta(startWatch, prog));
        }
        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, MILLISECONDS);
        logger.info("Processed all {} subjects in {}", subjectsCount, startWatch);

        for (Visitor visitor : visitors)
            visitor.end();
    }

    private String eta(Stopwatch watch, double progress) {
        long ms = (long)((watch.elapsed(MILLISECONDS) * (1 - progress))/progress);
        StringBuilder b = new StringBuilder();
        long val = DAYS.convert(ms, MILLISECONDS);
        if (val > 0) b.append(val).append("d");
        ms -= MILLISECONDS.convert(val, DAYS);

        val = HOURS.convert(ms, MILLISECONDS);
        if (val > 0) b.append(val).append("h");
        ms -= MILLISECONDS.convert(val, HOURS);

        val = MINUTES.convert(ms, MILLISECONDS);
        if (val > 0) b.append(val).append("m");
        ms -= MILLISECONDS.convert(val, MINUTES);

        val = SECONDS.convert(ms, MILLISECONDS);
        if (val > 0) b.append(val).append("s");
        ms -= MILLISECONDS.convert(val, SECONDS);

        val = MILLISECONDS.convert(ms, MILLISECONDS);
        if (val > 0) b.append(".").append(val);

        return b.toString();
    }


}
