package org.example.identifying;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.ResourceImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SubjectsFile {
    private int count;
    private final  @Nonnull File file;

    protected SubjectsFile(int count, @Nonnull File file) {
        this.count = count;
        this.file = file;
    }

    /**
     *
     * @return SubjectsFile or null if the file contains blank node ids
     * @throws IOException if cannot find or read file
     * @throws NumberFormatException if the numbers in file are corrupt
     */
    public static SubjectsFile open(@Nonnull File file) throws IOException {
        FileInputStream in = new FileInputStream(file);
        LineIterator it = IOUtils.lineIterator(in, StandardCharsets.UTF_8);
        try {
            String line = it.next();
            String[] pieces = line.split(",");
            boolean hasBlank = Boolean.parseBoolean(pieces[1]);
            return hasBlank ? null : new SubjectsFile(Integer.parseInt(pieces[0]), file);
        } finally {
            it.close();
            in.close();
        }
    }

    public static class FillProgress {
        public long total, done, subjects;
        public Stopwatch startWatch;

        public double progress() {
            return done/(double)total*100;
        }
    }

    public static
    SubjectsFile fill(@Nonnull File file, @Nonnull Model model,
                      @Nullable Consumer<FillProgress> progressConsumer) throws IOException {
        Set<Resource> subjects = new HashSet<>();
        StmtIterator it = model.listStatements();
        FillProgress progress = new FillProgress();
        progress.total = model.size();
        progress.startWatch = Stopwatch.createStarted();
        while (it.hasNext()) {
            Statement s = it.next();
            subjects.add(s.getSubject());
            progress.subjects = subjects.size();
            ++progress.done;
            if (progressConsumer != null) progressConsumer.accept(progress);
        }

        return write(file, subjects);
    }

    public static SubjectsFile openOrFill(@Nonnull File file,
                                          @Nonnull Model model,
                                          @Nullable Consumer<FillProgress> progressConsumer)
            throws IOException {
        if (file.exists() && file.length() > 0)
            return open(file);
        else
            return fill(file, model, progressConsumer);
    }

    public static SubjectsFile write(@Nonnull File file,
                                     @Nonnull Collection<Resource> subjects) throws IOException {
        boolean hasBlank = subjects.stream().anyMatch(Resource::isAnon);
        try (FileOutputStream fileOut = new FileOutputStream(file);
             PrintStream out = new PrintStream(fileOut)) {
            out.printf("%d,%b\n", subjects.size(), hasBlank);
            subjects.stream().map(Utils::resourceToString).forEach(out::println);
        }

        return new SubjectsFile(subjects.size(), file);
    }

    public int getCount() {
        return count;
    }

    public SampleIterator sample(int sampleSize) throws IOException {
        Preconditions.checkArgument(sampleSize <= count);
        Preconditions.checkArgument(sampleSize >= 0);

        int[] ints = new int[count];
        /* Fisher-Yates shuffle taken from https://stackoverflow.com/a/1520212 */
        Random rnd = ThreadLocalRandom.current();
        for (int i = ints.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ints[index];
            ints[index] = ints[i];
            ints[i] = a;
        }
        int[] sample = new int[sampleSize];
        System.arraycopy(ints, 0, sample, 0, sampleSize);
        return new SampleIterator(file, sample);
    }

    public static class SampleIterator implements Iterator<Resource>, Closeable {
        private FileInputStream inputStream;
        private LineIterator lineIterator;
        private final int[] indexes;
        private int indexesIdx;
        private int currentLine = 0;
        private Model model;

        public SampleIterator(@Nonnull File file,
                              @Nonnull int[] indexes) throws IOException {
            inputStream = new FileInputStream(file);
            lineIterator = IOUtils.lineIterator(inputStream, StandardCharsets.UTF_8);
            lineIterator.next(); //discard first line
            this.indexes = indexes;
            indexesIdx = 0;
        }

        @Override
        public boolean hasNext() {
            return indexesIdx < indexes.length;
        }

        @Override
        public Resource next() {
            int index = indexes[indexesIdx++];
            assert index >= currentLine : "index list MUST be sorted";
            while (currentLine < index) lineIterator.next();
            assert currentLine == index;

            Resource r = Utils.resourceFromString(lineIterator.next());
            if (model == null) return r;
            return r.isAnon() ? model.createResource(r.getId()) : model.createResource(r.getURI());
        }

        @Override
        public void close() throws IOException {
            lineIterator.close();
            inputStream.close();
        }

        public SampleIterator inModel(Model model) {
            this.model = model;
            return this;
        }
    }
}
