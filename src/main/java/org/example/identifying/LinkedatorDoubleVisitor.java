package org.example.identifying;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDFS;
import org.example.identifying.info.LinkedatorDoubleInfo;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;

public class LinkedatorDoubleVisitor extends LinkedatorVisitor<
        ImmutablePair<ImmutablePair<Property, Property>, Property>, LinkedatorDoubleInfo> {

    public LinkedatorDoubleVisitor(@Nonnull File csvOut,
                                   @Nonnull Function<String, QueryExecution> executionFac) {
        super(csvOut, executionFac);
    }

    private String template() {
        // ${subj} -- subject SPARQL term
        String linkTerm = link == null ? "" : "<" + link.getURI() + ">";
        if (lockIdentifying) {
            if (link == null) {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?p1 ?p2 ?link WHERE {" +
                        "  ${subj} ?p1 ?lit1; ?p2 ?lit2; ?link ?other; a/^rdfs:domain ?link \n" +
                        "    FILTER(isLiteral(?lit1) && isLiteral(?lit2) && ?p1 != ?p2 && ?other != ${subj}).\n" +
                        "  ?other ?p1 ?lit1; ?p2 ?lit2; a/^rdfs:range ?link.\n" +
                        "}";
            } else {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?p1 ?p2 WHERE {" +
                        "  ${subj} ?p1 ?lit1; ?p2 ?lit2; " + linkTerm + " ?other \n" +
                        "    FILTER(isLiteral(?lit1) && isLiteral(?lit2) && ?p1 != ?p2 && ?other != ${subj}).\n" +
                        "  ?other ?p1 ?lit1; ?p2 ?lit2.\n" +
                        "}";
            }
        } else {
            if (link == null) {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?p1 ?p2 ?link WHERE {" +
                        "  ${subj} ?p1 ?lit1; ?p2 ?lit2; ?link ?other; a/^rdfs:domain ?link \n" +
                        "    FILTER(isLiteral(?lit1) && isLiteral(?lit2) && ?p1 != ?p2 && ?other != ${subj}).\n" +
                        "  ?other ?q1 ?lit1; ?q2 ?lit2; a/^rdfs:range ?link FILTER(?q1 != ?q2).\n"+
                        "}";
            } else {
                return "PREFIX rdfs: <" + RDFS.getURI() + ">\n" +
                        "SELECT DISTINCT ?p1 ?p2 WHERE {" +
                        "  ${subj} ?p1 ?lit1; ?p2 ?lit2; " + linkTerm + " ?other \n" +
                        "    FILTER(isLiteral(?lit1) && isLiteral(?lit2) && ?p1 != ?p2 && ?other != ${subj}).\n" +
                        "  ?other ?q1 ?lit1; ?q2 ?lit2. FILTER(?q1 != ?q2)\n" +
                        "}";
            }
        }
    }

    @Override
    public void visit(Resource subject) {
        String queryStr = template().replace("${subj}", Utils.sparqlResource(subject));
        Utils.withQuery(queryStr, executionFactory, ex -> {
            HashMultimap<Property, Property> mm = HashMultimap.create();
            ResultSet results = ex.execSelect();
            while (results.hasNext()) {
                QuerySolution sol = results.next();
                Property p1 = sol.getResource("p1").as(Property.class),
                         p2 = sol.getResource("p2").as(Property.class),
                         link = this.link != null ? this.link
                                 : sol.getResource("link").as(Property.class);
                if (p1.getURI().compareTo(p2.getURI()) > 0) {
                    Property tmp = p1;
                    p1 = p2;
                    p2 = tmp;
                }
                if (mm.get(p1).contains(p2)) continue;
                mm.get(p1).add(p2);
                Literal lit1 = getLiteral(subject, p1), lit2 = getLiteral(subject, p2);
                updateIdentifying(subject, true,
                        ImmutablePair.of(ImmutablePair.of(p1, p2), link),
                        LinkedatorDoubleInfo::new, lit1, lit2);
            }
            return null;
        });
    }

    private Literal getLiteral(Resource subject, Property property) {
        NodeIterator it = subject.getModel().listObjectsOfProperty(subject, property);
        try {
            while (it.hasNext()) {
                RDFNode node = it.next();
                if (node.isLiteral()) return node.asLiteral();
            }
        } finally {
            it.close();
        }
        return null;
    }

    @Override
    public void end() throws IOException {
        super.end(LinkedatorDoubleInfo.getHeaders(),
                Comparator.comparing(LinkedatorDoubleInfo::getHits).reversed());
    }
}
