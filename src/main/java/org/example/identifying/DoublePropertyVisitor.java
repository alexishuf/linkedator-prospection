package org.example.identifying;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.example.identifying.info.DoubleInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DoublePropertyVisitor
        extends InfoMapPropertyVisitor<ImmutablePair<Property, Property>, DoubleInfo> {
    private static final Logger logger = LoggerFactory.getLogger(DoublePropertyVisitor.class);
    protected Function<String, QueryExecution> executionFactory;
    private Set<Property> whitelist = null;
    private int maxPairs = Integer.MAX_VALUE;

    public DoublePropertyVisitor(File csvOut, Function<String, QueryExecution> executionFactory) {
        super(csvOut);
        this.executionFactory = executionFactory;
    }

    public DoublePropertyVisitor setWhitelist(Set<Property> whitelist) {
        this.whitelist = whitelist;
        return this;
    }

    public void setMaxPairs(int maxPairs) {
        this.maxPairs = maxPairs;
    }

    private List<ImmutablePair<Property, Property>> getPairs(Resource subject) {
        List<Property> properties = subject.listProperties().toList().stream()
                .filter(s -> s.getObject().isLiteral())
                .map(Statement::getPredicate)
                .filter(p -> whitelist == null || whitelist.contains(p))
                .distinct().collect(Collectors.toList());
        List<ImmutablePair<Property, Property>> pairs = new ArrayList<>();
        Collections.shuffle(pairs);
        for (int i = 0; i < properties.size(); i++) {
            for (int j = i+1; j < properties.size(); j++) {
                pairs.add(ImmutablePair.of(properties.get(i), properties.get(j)));
                if (pairs.size() >= maxPairs)
                    return pairs;
            }
        }
        return pairs;
    }

    @Nonnull
    private Literal getLiteral(Resource subject, Property property) {
        for (StmtIterator it = subject.listProperties(property); it.hasNext(); ) {
            RDFNode o = it.next().getObject();
            if (o.isLiteral()) return o.asLiteral();
        }
        throw new NoSuchElementException();
    }

    @Override
    public void visit(Resource subject) {
        for (ImmutablePair<Property, Property> pair : getPairs(subject)) {
            Literal l1 = getLiteral(subject, pair.left), l2 = getLiteral(subject, pair.right);
            String lex1 = Utils.sparqlLiteral(l1), lex2 = Utils.sparqlLiteral(l2);

            String queryStr = String.format("SELECT DISTINCT ?s WHERE {\n" +
                    "  ?s <%1$s> %2$s; <%3$s> %4$s.\n" +
                    "} LIMIT 2", pair.left.getURI(), lex1, pair.right.getURI(), lex2);
            Boolean is = Utils.withQuery(queryStr, executionFactory, ex -> {
                ResultSet results = ex.execSelect();
                while (results.hasNext()) {
                    Resource s = results.next().getResource("s");
                    if (s != subject) return true;
                }
                return false;
            });
            updateIdentifying(subject, is, pair, DoubleInfo::new, l1, l2);
        }
    }

    @Override
    public void end() throws IOException {
        super.end(DoubleInfo.getHeaders(), Comparator.comparing(DoubleInfo::hitRate).reversed());
    }
}
