package org.example.identifying;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.rdf.model.Property;
import org.example.identifying.info.Info;

import javax.annotation.Nullable;
import java.io.File;
import java.util.function.Function;

public abstract class LinkedatorVisitor<K, V extends Info> extends InfoMapPropertyVisitor<K, V> {
    protected Function<String, QueryExecution> executionFactory;
    protected Property link = null;
    protected boolean lockIdentifying = false;

    public LinkedatorVisitor(File csvOut, Function<String, QueryExecution> executionFactory) {
        super(csvOut);
        this.executionFactory = executionFactory;
    }

    /**
     * Forces the link between resources to be the given property. By default the visitor tries
     * to find any property between the visited subject and other resource..
     */
    public void setLink(@Nullable Property link) {
        this.link = link;
    }

    /**
     * If the identifying property is locked, the visitor will look for resources linked to the
     * visited subject with the sharing both the value and the property for each property-value
     * pair of the subject. If set to false, the it will suffice for the resources to share
     * a value,
     */
    public void lockIdentifying(boolean lockIdentifying) {
        this.lockIdentifying = lockIdentifying;
    }
}
